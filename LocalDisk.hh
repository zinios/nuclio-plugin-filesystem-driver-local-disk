<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\driver\localDisk
{
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\fileSystem\driver\localDisk\LocalDiskException;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;
	
	/**
	 * Manage file system functionalities on normal local linux files.
	 *
	 * This class provides the ability to manage CURD over files hosted on the server hard drive
	 * 
	 */
	<<provides('fileSystem::local-disk')>>
	class LocalDisk extends Plugin implements CommonInterface
	{
		public static function getInstance(/* HH_FIXME[4033] */...$args):LocalDisk
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
		 * Check if the file exist or not.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return bool       	True/False for file existance.
		 */
		public function exists(string $path):bool
		{
			return file_exists($path);
		}

		/**
		 * Check if the path given is a file.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile(string $path):bool
		{
			return is_file($path);
		}

		/**
		 * Get content of a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function read(string $path):string
		{
			if($this->isFile($path))
			{
				$content=file_get_contents($path);
				return $content;
			}
			else
			{
				return 'Not a file!';
			}
		}

		/**
		 * Overwrite file or create if does not exist.
		 * 
		 * @access public
		 * 
		 * @param  string $path     Path to the file.
		 * @param  string $contents Content to be added to the file.
		 * @return bool          	True/False for the write process.
		 */
		public function write(string $path, string $contents):bool
		{
			return (bool)file_put_contents($path, $contents);
		}

		/**
		 * Append content to a file.
		 * 
		 * @access public
		 * 
		 * @param  string $path 	Path to the file.
		 * @param  string $contents Content to be append
		 * @return bool   	 		True/False for the append process.
		 */
		public function append(string $path, string $contents):bool
		{
			return (bool)file_put_contents($path, $contents, FILE_APPEND);
		}

		/**
		 * Creates an empty file at the passed path
		 * 
		 * @access public
		 * 
		 * @param  string $path the path to the file to be created
		 * @return bool       	true/false on success/failure
		 */
		public function touch(string $path):bool
		{
			return (bool)touch($path);
		}

		/**
		 * Delete file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file to be deleted.
		 * @return mixed       	True if delete success, error message if failed.
		 */
		public function delete(string $path, int $rules):bool
		{
			if ($this->isFile($path))
			{
				if (unlink($path))
				{
					return true;
				}
				throw new LocalDiskException('Something happen, cant delete file.');
			}
			throw new LocalDiskException('File does not Exist!');
		}

		/**
		 * Move file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be moved.
		 * @param  string $target 	Where to move.
		 * @return bool         	True/False for the move process.
		 */
		public function move(string $path, string $target, int $rules):bool
		{
			return rename($path, $target);
		}

		/**
		 * Copy file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be copied.
		 * @param  string $target 	Where to copied.
		 * @return bool         	True/False for the copied process.
		 */
		 
		public function copy(string $path, string $target):bool
		{
			return copy($path, $target);
		}

		/**
		 * Get the file name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName(string $path):?string
		{
			return pathinfo($path, PATHINFO_FILENAME);
		}

		/**
		 * Get the file extension.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt(string $path):?string
		{
			return pathinfo($path, PATHINFO_EXTENSION);
		}

		/**
		 * Get the file type.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType(string $path):?string
		{
			return filetype($path);
		}

		/**
		 * Get the file size.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize(string $path):int
		{
			return filesize($path);
		}

		/**
		 * Check whether the param given is path or not.
		 * 
		 * @access public
		 * 
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory(string $directory):bool
		{
			return is_dir($directory);
		}

		public function isEmpty(string $directory):bool
		{
			return (count(scandir($directory)) == 2);
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable(string $path):bool
		{
			return is_writable($path);
		}

		/**
		 * List all the files in the direotory.
		 * 
		 * @access public
		 * 
		 * @param  string $directory 	Path of directory
		 * @return ImmVector  			List of files.
		 */
		public function listAllFilesAndFolders(string $directory):Vector<string>
		{
			return new Vector(scandir($directory));
		}

		/**
		 * Create Directory
		 * 
		 * @access public
		 * 
		 * @param  string       $path      	Where to create directory.
		 * @param  int  		$mode      	File mode. Eg. 0777 for read/write/execute
		 * @param  bool 		$recursive 	Allows the creation of nested directories specified in the path
		 * @return bool                 	True/False for file created or not.
		 */
		public function create(string $path, int $mode = 0755, bool $recursive = false):bool
		{
			return mkdir($path, $mode, $recursive);
		}
		
		
		/**
		 * Determine if readable.
		 *
		 * @access public
		 * 
		 * @param      string   $path   path for the file
		 *
		 * @return     boolean  True if readable, False otherwise.
		 */
		public function isReadable(string $path):bool
		{
			return is_readable($path);
		}
		
		/**
		 * Look for files or folders in a specific path
		 * 
		 * @access public
		 * 
		 * @param  string $path  Path to perform the search into.
		 * @param  string $regex regex expression to be matched
		 * @return string List of all the search results
		 */
		public function find(string $path, string $regex):Vector<string>
		{
			return new Vector(null);
		}

		/**
		 * Delete all files inside directory.
		 * 
		 * @access pubilic
		 * 
		 * @param  string $directory 	Path to directories
		 * @return bool            		True/False for successfully deleted.
		 */
		public function deleteFilesInDir(string $directory):bool
		{
			if (!$this->isDirectory($directory)) 
			{
				return false;	
			}

			$files = glob($directory.'/*');
			foreach($files as $file)
			{
				if($this->isFile($file))
				{
					unlink($file);
				}
			}
			return true;
		}
	}
}
